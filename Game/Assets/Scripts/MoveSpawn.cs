﻿using UnityEngine;

public class MoveSpawn : MonoBehaviour
{
    public GameObject move;
    private bool cont1 = true;
    private int cont2 = 0;

    void Update()
    {

        if (cont1 == true)
        {
			move.transform.Translate(Vector3.right * 2f * Time.deltaTime);
            cont2++;
            if (cont2 == 50)
                cont1 = false;
        }

        if (cont1 == false)
        {
			move.transform.Translate(Vector3.left * 2f * Time.deltaTime);
            cont2--;
            if (cont2 == -50)
                cont1 = true;
        }
    }
}
