﻿using UnityEngine.UI;
using UnityEngine;

public class Points : MonoBehaviour 
{
	public Text score;
	static public int puntos = 0;
    public Collider[] coli;

	void Start()
	{
		FindObjectOfType<Text>().text = "Score " + puntos;
		//puntos = 0;
	}

	void OnCollisionEnter(Collision col)
	{
		int col_num = Random.Range (0, 5);

		if (coli[col_num].tag == "cubo" || coli[col_num].tag == "Player")
		{
			puntos += 1;
			FindObjectOfType<Text>().text = "Score " + puntos;
			print ("toco");
		}

		print (puntos);
	}
}
