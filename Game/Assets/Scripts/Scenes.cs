﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class Scenes : MonoBehaviour 
{	
	public string sceneName;
	public GameObject pause;

	public void OnMouseDown()
	{
		SceneManager.LoadScene (sceneName: sceneName);
	}

	public void Exit()
	{
		Application.Quit ();
	}
}