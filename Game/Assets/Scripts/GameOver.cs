﻿using UnityEngine.SceneManagement;
using UnityEngine;

public class GameOver : MonoBehaviour
{
    void OnTriggerEnter(Collider col)
    {
		SceneManager.LoadScene("Lose");
    }
}
